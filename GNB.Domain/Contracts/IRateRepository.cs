﻿using GNB.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Domain.Contracts
{
    public interface IRateRepository
    {
        IEnumerable<Rate> GetRates();
    }
}
