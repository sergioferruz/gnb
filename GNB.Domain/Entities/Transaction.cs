﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Domain.Entities
{
    public class Transaction
    {
        public string sku { get; set; }

        public string currency { get; set; }

        public double amount { get; set; }
    }
}
