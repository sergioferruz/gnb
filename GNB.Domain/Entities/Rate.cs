﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Domain.Entities
{
    public class Rate
    {
        public string from { get; set; }
        public string to { get; set; }
        public double rate { get; set; }
    }
}
