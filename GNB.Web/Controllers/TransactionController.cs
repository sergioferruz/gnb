﻿using GNB.Application.Services;
using GNB.Domain.Entities;
using GNB.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace GNB.Web.Controllers
{
    public class TransactionController : ApiController
    {       
        public IEnumerable<TransactionViewModel> Get() 
        {
            var transactionList = new TransactionService().GetTransactions();
            List<TransactionViewModel> viewModel = new List<TransactionViewModel>();
            foreach (Transaction item in transactionList) 
            {
                viewModel.Add(new TransactionViewModel
                {
                    sku = item.sku,
                    amount = item.amount,
                    currency = item.currency
                });
            }
            return viewModel;
        }

        public IEnumerable<TransactionViewModel> Get(string sku)
        {
            var transactionList = new TransactionService().ConvertTransactions(sku);
            List<TransactionViewModel> viewModel = new List<TransactionViewModel>();
            foreach (Transaction item in transactionList)
            {
                viewModel.Add(new TransactionViewModel
                {
                    sku = item.sku,
                    amount = item.amount,
                    currency = item.currency
                });
            }
            return viewModel;
        }
    }
}
