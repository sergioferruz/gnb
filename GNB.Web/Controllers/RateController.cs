﻿using GNB.Application.Services;
using GNB.Domain.Entities;
using GNB.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GNB.Web.Controllers
{
    public class RateController : ApiController
    {
        public IEnumerable<RateViewModel> Get() 
        {
            var rateList = new RateService().GetRates();
            List<RateViewModel> viewModel = new List<RateViewModel>();
            foreach (Rate item in rateList) 
            {
                viewModel.Add(new RateViewModel
                {
                    from = item.from,
                    to = item.to,
                    rate = item.rate
                });
            }
            return viewModel;
        }
    }
}
