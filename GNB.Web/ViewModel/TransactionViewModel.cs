﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GNB.Web.ViewModel
{
    public class TransactionViewModel
    {
        public string sku { get; set; }
        public string currency { get; set; }
        public double amount { get; set; }
    }
}