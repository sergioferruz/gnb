﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GNB.Web.ViewModel
{
    public class RateViewModel
    {
        public string from { get; set; }
        public string to { get; set; }
        public double rate { get; set; }
    }
}