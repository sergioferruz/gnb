﻿using GNB.Domain.Contracts;
using GNB.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GNB.Infrastructure.Repository
{
    public class TransactionRepository : ITransactionRepository
    {

        //Attributes
        String url = "http://quiet-stone-2094.herokuapp.com/transactions.xml";
        XmlTextReader reader;
        Transaction transaction;
        List<Transaction> transactionList = new List<Transaction>();
        List<Rate> rateList = new List<Rate>();
        IRateRepository rateRepository = new RateRepository();
        
        public IEnumerable<Transaction> ConvertTransactions(string sku)
        {
            //Saving rates attributes
            double rateEurUsd = 0;
            double rateEurCad = 0;
            double rateUsdEur = 0;          
            double rateCadEur = 0;

            try
            {

                //Currency converter
                rateList = rateRepository.GetRates().ToList();
                transactionList = GetTransactions().ToList();             
                transactionList = transactionList.Where(x => x.sku == sku).ToList();

                foreach (Transaction item in transactionList)
                {
                    switch (item.currency)
                    {
                        case "CAD":
                            foreach (Rate rate in rateList)
                            {
                                if (rate.from == "CAD" && rate.to == "EUR")
                                {
                                    rateCadEur = rate.rate;
                                }
                                else if (rate.from == "EUR" && rate.to == "CAD")
                                {
                                    rateEurCad = rate.rate;
                                }
                                else if (rate.from == "USD" && rate.to == "EUR" || rate.from == "EUR" && rate.to == "USD")
                                {
                                    if (rate.from == "USD" && rate.to == "EUR")
                                    {
                                        rateUsdEur = rate.rate;                                    
                                    }
                                    else if (rate.from == "EUR" && rate.to == "USD")
                                    {
                                        rateEurUsd = rate.rate;  
                                    }
                                }
                            }
                            item.amount = item.amount * rateCadEur;
                            break;
                        case "USD":
                            foreach (Rate rate in rateList)
                            {
                                if (rate.from == "USD" && rate.to == "EUR")
                                {
                                    rateUsdEur = rate.rate;
                                }
                                else if (rate.from == "EUR" && rate.to == "USD")
                                {
                                    rateEurUsd = rate.rate;
                                }
                                else if (rate.from == "CAD" && rate.to == "EUR" || rate.from == "EUR" && rate.to == "CAD")
                                {
                                    if (rate.from == "CAD" && rate.to == "EUR")
                                    {
                                        rateCadEur = rate.rate;                                   
                                    }
                                    else if (rate.from == "EUR" && rate.to == "CAD")
                                    {
                                        rateEurCad = rate.rate; 
                                    }
                                }
                            }
                            item.amount = item.amount * rateUsdEur;
                            break;
                    }
                }
                return transactionList;
            }
            catch (Exception e) 
            {
                throw new Exception("Unable to get Transactions, please check that the SKU you entered is correct", e);
            }
        }

        public IEnumerable<Transaction> GetTransactions()
        {
            reader = new XmlTextReader(url);


            // Xml Lector
            try
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            transaction = new Transaction();
                            while (reader.MoveToNextAttribute())
                            {
                                if (reader.Name == "sku")
                                {
                                    transaction.sku = reader.Value;
                                }
                                else if (reader.Name == "amount")
                                {
                                    transaction.amount = Convert.ToDouble(reader.Value);
                                }
                                else if (reader.Name == "currency")
                                {
                                    transaction.currency = reader.Value;
                                }
                            }
                            if (reader.Name != "transactions")
                            {
                                transactionList.Add(transaction);
                            }
                            break;
                        case XmlNodeType.Text:
                            break;
                        case XmlNodeType.EndElement:
                            break;
                    }
                }
                return transactionList;
            }
            catch (Exception e) 
            {
                throw new Exception("Unable to get Transactions", e);
            }
        }
    }
}
