﻿using GNB.Domain.Contracts;
using GNB.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GNB.Infrastructure.Repository
{
    public class RateRepository : IRateRepository
    {
        //Attributes 
        String url = "http://quiet-stone-2094.herokuapp.com/rates.xml";
        XmlTextReader reader;
        Rate rate;
        List<Rate> rateList = new List<Rate>();

        //Xml Lector
        public IEnumerable<Rate> GetRates()
        {
            reader = new XmlTextReader(url);

            try
            {

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            rate = new Rate();
                            while (reader.MoveToNextAttribute())
                            {
                                if (reader.Name == "from")
                                {
                                    rate.from = reader.Value;
                                }
                                else if (reader.Name == "to")
                                {
                                    rate.to = reader.Value;
                                }
                                else if (reader.Name == "rate")
                                {
                                    rate.rate = Convert.ToDouble(reader.Value);
                                }
                            }
                            if (reader.Name != "rates")
                            {
                                rateList.Add(rate);
                            }
                            break;
                        case XmlNodeType.Text:
                            break;
                        case XmlNodeType.EndElement:
                            break;
                    }
                }
                return rateList;
            }
            catch (Exception e) 
            {
                throw new Exception("Unable to get Rates", e);
            }
        }
    }
}
