﻿using GNB.Domain.Contracts;
using GNB.Domain.Entities;
using GNB.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Application.Services
{
    public class RateService
    {
        readonly IRateRepository rateRepository;
        public RateService() 
        {
            rateRepository = new RateRepository();
        }
        public IEnumerable<Rate> GetRates() 
        {
            return rateRepository.GetRates();
        }
    }
}
