﻿using GNB.Domain.Contracts;
using GNB.Domain.Entities;
using GNB.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GNB.Application.Services
{
    public class TransactionService
    {
        readonly ITransactionRepository transactionRepository;
        public TransactionService() 
        {
            transactionRepository = new TransactionRepository();
        }
        public IEnumerable<Transaction> GetTransactions() 
        {
            return transactionRepository.GetTransactions();
        }
        public IEnumerable<Transaction> ConvertTransactions(string sku) 
        {
            return transactionRepository.ConvertTransactions(sku);
        }
    }
}
